import numpy as np
from PIL import ImageGrab
import cv2
import time
from directx_keys import PressKey, ReleaseKey, W, S, A, D
from grab_screen import ScreenGrab

game_title = 'Grand Theft Auto V'
screen_grab = ScreenGrab(game_title)

def crop_roi(img, vertices):
    mask = np.zeros_like(img)
    cv2.fillPoly(mask, vertices, 255)
    masked = cv2.bitwise_and(img, mask)
    return masked

def process_img(img):
    processed_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    processed_img = cv2.Canny(processed_img, 100, 300)
    vertices = np.array( [[[10, 500], [10, 300], [300, 200], [500, 200], [800, 300], [800, 500]]] )
    processed_img = crop_roi(processed_img, vertices)
    return processed_img

waiting_time = 4
for i in range(waiting_time):
    print(waiting_time-i)
    time.sleep(1)

PressKey(W)
time.sleep(3)
ReleaseKey(W)

last_time = time.time()
while(True):
    screen, bbox = screen_grab.GetScreen()
    processed_screen = process_img(screen)
    print('Loop took {} second'.format(time.time() - last_time))
    last_time = time.time()

    winname = 'Original Image'
    cv2.namedWindow(winname)        # Create a named window
    cv2.moveWindow(winname, bbox[2]*2, 0)  # Move it to (40,30)
    #cv2.imshow(winname, cv2.cvtColor(screen, cv2.COLOR_BGR2RGB))

    winname = 'Processed Image'
    cv2.namedWindow(winname)        # Create a named window
    cv2.moveWindow(winname, bbox[2], bbox[0])  # Move it to (40,30)
    cv2.imshow(winname, processed_screen)

    if cv2.waitKey(25) & 0xFF == ord('q'):
        cv2.destroyAllWindows()
        break
