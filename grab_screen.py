from PIL import ImageGrab
import numpy as np
import win32gui

class ScreenGrab:
    def __init__(self, title):
        my_window = win32gui.FindWindow(0, title)
        win32gui.SetForegroundWindow(my_window)
        self.bbox = win32gui.GetWindowRect(my_window)
        self.new_bbox = 0, 0, self.bbox[2] - self.bbox[0], self.bbox[3] - self.bbox[1]
        win32gui.MoveWindow(my_window, 0, 0, self.bbox[2] - self.bbox[0], self.bbox[3] - self.bbox[1], 0)

    def GetScreen(self):
        border_size = 27
        bbox_without_border = self.new_bbox[0], self.new_bbox[1] + border_size, self.new_bbox[2], self.new_bbox[3]
        return np.array(ImageGrab.grab(bbox_without_border)), self.new_bbox

